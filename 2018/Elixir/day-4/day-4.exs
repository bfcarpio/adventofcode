defmodule Day4 do
  
  def load_inputs(file_name) do
    File.read!(file_name)
  end

  def part1(inputs) do
    inputs
    |> process_data()
    # Sort by total time asleep
    |> Enum.max_by(&elem(&1, 1))
    |> multiply_id_by_minute()
  end

  def part2(inputs) do
    inputs
    |> process_data()
    # Sort by occurrences of most common minute asleep
    |> Enum.max_by(fn {_, _, {_, x}} -> x end)
    |> multiply_id_by_minute()
  end

# Private functions

# Common actions taking in both public functions
  def process_data(inputs) do
    parse_data(inputs)
    |> Enum.reduce({0, 0, %{}}, &group_minutes/2)
    |> elem(2)
    |> compute_time_and_minutes()
  end

# Interpret data from the string input
  defp parse_data(inputs) do
    format_data = fn
      # This is the dictionary use of an atom. Repeatable key-like function
      ([_, date_time, minute, action]) -> {String.to_atom(action), date_time, String.to_integer(minute)}
      ([_, date_time, _, _, id]) -> {:new, date_time, String.to_integer(id)}
    end

    inputs
    # Pull put all the necessary information into easily parsed lists.
    # Vale in each pair of parenthesis becomes an item in the returned list
    |> (&Regex.scan(~r/\[(\d+-\d+-\d+\s\d+:(\d+))]\s(falls|wakes|Guard #(\d+))/, &1)).()
    # Reformat the list into tuples with uniform value placement
    |> Enum.map(format_data)
    # Sort by the time and date
    |> Enum.sort_by(&elem(&1, 1))
  end

# Assign the minutes to an elf_id
  defp group_minutes({action, _dt, val}, {id, start, acc}) do
    update_acc = fn id, start, val -> 
      # Make a list of all the minutes that they have been asleep: [22, 23, 24]
      sleep_minutes = Enum.to_list(start..val-1)
      # Add to map of elf_id => [list of minutes been asleep]
      # According to erlang efficiency docs it seems that `++` is faster than flatten
      # To maintain structure of list, it's simple and fast to just concatenate
      Map.update(acc, id, sleep_minutes, &(&1 ++ sleep_minutes))
    end

    case action do
      :new -> {val, start, acc}
      :falls -> {id, val, acc}
      :wakes -> {id, start, update_acc.(id, start, val)}
    end
  end

# Math on total minutes slept and what minute is the most common
  defp compute_time_and_minutes(data) do
    find_common_minute = fn v ->
      Enum.reduce(v, %{}, fn x, acc -> Map.update(acc, x, 1, &(&1 + 1)) end)
      |> Enum.max_by(&elem(&1, 1))
    end

    # Transform everything into tuple with {elf_id, total minutes asleep, {most common minute, occurrences}}
    Enum.map(data, fn {k, v} -> {k, Kernel.length(v), find_common_minute.(v)} end)
  end

  defp multiply_id_by_minute({id, _, {common_minute, _}}) do
    id * common_minute
  end

end